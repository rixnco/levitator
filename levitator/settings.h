#ifndef __SETTINGS_H__
#define __SETTINGS_H__


// Datastructure to hold the various controller's parameters.
typedef struct {
  // PID parameters
  int   setPoint;
  float kp;
  float ki;
  float kd;

  // Control Parameters
  unsigned int sensFilter;
  bool         sensReversed;
  unsigned int coilFilter;
  unsigned int ctrlPeriod;
  unsigned int actvThreshold;
  unsigned int actvTimeout;

  byte coilMin;
  byte coilMax;
  byte coilMid;
  
} settings_t;

// The global settings variable
extern settings_t settings;

/**
 * Write settings to EEPROM
 */
void write_settings();

/**
 * Read the settings from EEPROM
 * Return true if successful
 * false if version or checksum fails
 */
bool read_settings();

/**
 * Restore the settings to compile time's defaults values.
 * Does not write them to EEPROM.
 */
void default_settings();


#endif

