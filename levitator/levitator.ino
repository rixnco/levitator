  /*******************************************************************************************
 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 * >>>>>>>>>>>>>>>>>>>>>>>> Levitator PID Controller By RxBConcept <<<<<<<<<<<<<<<<<<<<<<<<<
 * 
 * Yet another magnetic levitation controller
 * 
 * This controller is implemented using a free running ADC converter, to follow the hall 
 * effect sensor readout in "real time"
 * 
 * Timer 2 is setup to sequence the controller at a 1ms rate.
 * The PID computation is performed every "ctrlPeriod" milliseconds (see config.h)
 * The control loop is thus totally independant from the main processing loop.
 * 
 * The rest of the time, the system checks and process the serial protocol.
 * 
 * Check "Config.h" for more details on configuring the Levitator
 * 
 * The Levitator protocol is a simple ASCII protocol, so you can configure and control the
 * Levitator using a simple serial terminal
 * 
 * Protocol:
 * 
 * "$$"       : List all settings parameters
 * "$xxx"     : print the parameter 'xxx'
 * "$xxx=yyy" : set the the 'xxx' parameter's value to 'yyy'
 * "$<"       : Write current settings to EEPROM. The Levitator will use those settings every time it is powerd on.
 * "$>"       : Read settings from EEPROM.
 * 
 * "![SP][,[KP][,[KI][,[KD]]]] : set the PID 'setPoint', 'kP', 'kI', 'kD' to the corresponding values at once.
 *                               Values between sqared brackets are optional.
 * 
 * "?"        : Request the current controller state: >IDLE/ACTV,setpoint,sensor,coil
 * "?x"       : Request a report every 'x' milliseconds. 0 to turn off reporting.
 * 
 * The controller acknoledge requests with
 * ">OK"
 * or
 * ">ERROR,msg"
 * 
 * Enjoy :-)
 */



#include "levitator.h"
#include "config.h"
#include "protocol.h"
#include "settings.h"


#define TIMER2_1MS_DELAY    (256 - (16000/128))   // 1ms delay
#define ROUND(x) ((int)((x)+0.5))


// The Controller state
volatile bool ctrlState=true;

// PID computation period counter 
volatile unsigned long ctrlPeriodCounter     = 0;

// Active timeout watchdog counter
volatile unsigned long activeTimeoutCounter   = 0;


volatile unsigned long reportPeriod    = 0;
volatile unsigned long reportCounter   = 0;
volatile bool reportFlag=false;


// Current sensor value.
volatile int  currentSensor = 1024; 

//Last sensor value used to calculate the derivative term
volatile int  lastSensor = 1024;

// The running error over time
volatile int  integralError = 0;  

//Current PWM duty cycle for the coil
volatile int coilPWM = 0; 




// Private sensor value managed by ADC interrupt routine
// Access it using the readSensor() function.
volatile unsigned int _sensor=0;

#define readSensor() _sensor

// Return the latest sensor value provided
// by the free running adc conversion.
//int readSensor() {
//  int v;
//  //noInterrupts();
//  v= _sensor;
//  //interrupts();
//  return v;
//}

void writeCoilPWM(int value) {
#if COIL_PIN == 9
    OCR1A = value;  
#elif COIL_PIN == 10
    OCR1B = value;
#endif
}

//
// Manage controller's state transitions
//
void changeState(bool active) {
  if(ctrlState==active) return;
  digitalWrite(LED_PIN, active?LOW:HIGH);
  ctrlState=active;
}


// Idle state
void doIdle()
{
  // Read sensor
  currentSensor = readSensor();
  
  //Transition back to active mode if there's a magnet in detection range
  if(settings.actvThreshold > currentSensor) {
    lastSensor = currentSensor; // Clear the derivative term.
    changeState(true);
  }
}

// Active state
void doActive()
{
  // Read sensor
  currentSensor = ROUND(((currentSensor * (settings.sensFilter - 1)) + readSensor()) / settings.sensFilter);

  if(settings.actvThreshold <= currentSensor) {
    //We don't see a permanent magnet right now. Check watchdog timeout.
    if(activeTimeoutCounter==0) {
      // Turn off coil
      coilPWM=0;
      writeCoilPWM(0);
      // Switch to IDLE state
      changeState(false);
    }
    return; 
  } 
  //There is a permanent magnet in range during this update
  // Rearm active timeout watchdog
  activeTimeoutCounter=settings.actvTimeout;
  
  int error = settings.setPoint - currentSensor; //Difference between current and expected values (for proportional term)

  //Slope of the input over time (for derivative term). This is called Derivative on Measurement, as opposed to the more normal Derivative on Error. Used to reduce "derivative kick" when changing the set point, not a huge deal at our frequency
  int dError = currentSensor - lastSensor; 
  
  integralError = constrain(integralError + error, -MAX_INTEGRAL, MAX_INTEGRAL); //Roughly constant error over time (for integral term)

  //Store for next calculation of dError
  lastSensor = currentSensor;


  // Filter out coil response. Higher value take longer to stabilize
  int currentPWM = settings.coilMid - (ROUND((settings.kp*error) - (settings.kd*dError) + (settings.ki*integralError)));
  coilPWM = ROUND(((coilPWM * (settings.coilFilter-1)) + currentPWM ) / settings.coilFilter);
  
  //It's possible to overshoot in the above, so constrain to between our max and min
  coilPWM = constrain(coilPWM, settings.coilMin, settings.coilMax);
  
  writeCoilPWM(coilPWM);
 
}



// Interrupt service routine for the ADC completion
ISR(ADC_vect){
  unsigned int s;
  // Store sensor value
  // Must read low first
  s = ADCL | (ADCH << 8);
  _sensor=settings.sensReversed?1024-s:s;
  // Not needed because free-running mode is enabled.
  // Set ADSC in ADCSRA (0x7A) to start another ADC conversion
  // ADCSRA |= B01000000;
}


// Interrupt service routine for the timer2 overflow
ISR(TIMER2_OVF_vect) {
  // Rearm timer2
  TCNT2 = TIMER2_1MS_DELAY;

  // Manage PID Control Loop
  if(--ctrlPeriodCounter==0) {
    // PID Control loop.
    if(ctrlState) {
      doActive();
    }else {      
      doIdle();
    }
    ctrlPeriodCounter= settings.ctrlPeriod;
  }

  // Decrease active timeout watchdog counter
  if(activeTimeoutCounter>0) --activeTimeoutCounter;

  // Manage periodic reports
  if(reportCounter>0) {
    --reportCounter;
  } else {
    if(reportPeriod>0) {
      // Report time !!
      // Set report flag
      reportFlag=true;
      // Rearm counter
      reportCounter=reportPeriod;
    }
  }
}



void setup()
{ 
   
  // Setup led 
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);

  // Setup coil
  pinMode(COIL_PIN, OUTPUT);
  digitalWrite(COIL_PIN, LOW);

  // Setup Sensor
  pinMode(SENSOR_PIN, INPUT);

  // Setup VUSB Sense pin
  pinMode(VUSB_PIN, INPUT);
  int val=0;
  val=analogRead(VUSB_PIN);

  if(val<950) {
    // Enable BT Module if present
    pinMode(BTEN_PIN, OUTPUT);
    digitalWrite(BTEN_PIN, HIGH);
  }

  noInterrupts(); // Disable interrupts

  // Setup timer 1 as Phase Correct non-inverted PWM, 31372.55 Hz.
  // WGM10 is used for Phase Correct PWM, COM1A1/COM1B1 sets output to non-inverted
  TCCR1A = 0;
  TCCR1A = _BV(COM1A1) | _BV(COM1B1) | _BV(WGM10);
  // PWM frequency is 16MHz/255/2/<prescaler>, prescaler is 1 here by using CS10
  TCCR1B = 0;
  TCCR1B = _BV(CS10);


  // Setup Timer2 for 1ms interrupt
  TIMSK2 &= ~(_BV(TOIE2));              // Disable Timer2 interrupts
  TCCR2A &= ~(_BV(WGM21) | _BV(WGM20));
  TCCR2B &= ~(_BV(WGM22));
  TCCR2B |= ((1<<CS22) | (1<<CS20));    // Prescaler 128
  TCCR2B &= ~(1<<CS21);                 // Prescaler 128
  ASSR &= ~(_BV(AS2));
  TIMSK2 &= ~(_BV(OCIE2A));


  // Setup ADC for free running conversions
  // clear ADLAR in ADMUX (0x7C) to right-adjust the result
  // ADCL will contain lower 8 bits, ADCH upper 2 (in last two bits)
  ADMUX &= ~(_BV(ADLAR)); //B11011111;
  
  // Set REFS1..0 in ADMUX (0x7C) to change reference voltage to the
  // proper source (01)
  ADMUX &= ~(_BV(REFS1));
  ADMUX |=  (_BV(REFS0));
  
  // Clear MUX3..0 in ADMUX (0x7C) in preparation for setting the analog
  // input
  ADMUX &= B11110000;
  
  // Set MUX3..0 in ADMUX (0x7C) to read from AD0
  // Do not set above 15! You will overrun other parts of ADMUX. A full
  // list of possible inputs is available in Table 24-4 of the ATMega328
  // datasheet
  //ADMUX |= 0;
  
  
  // Set ADEN in ADCSRA (0x7A) to enable the ADC.
  // Note, this instruction takes 12 ADC clocks to execute
  ADCSRA |= _BV(ADEN); //B10000000;
  
  // Set ADATE in ADCSRA (0x7A) to enable auto-triggering.
  ADCSRA |= _BV(ADATE); //B00100000;
  
  // Clear ADTS2..0 in ADCSRB (0x7B) to set trigger mode to free running.
  // This means that as soon as an ADC has finished, the next will be
  // immediately started.
  ADCSRB &= B11111000;
  
  // Set the Prescaler to 128 (16000KHz/128 = 125KHz)
  // Above 200KHz 10-bit results are not reliable.
  ADCSRA |= B00000111;
  
  // Set ADIE in ADCSRA (0x7A) to enable the ADC interrupt.
  // Without this, the internal interrupt will not trigger.
  ADCSRA |= B00001000;
  
  
  // Kick off the first ADC
  // Set ADSC in ADCSRA (0x7A) to start the ADC conversion
  ADCSRA |= _BV(ADSC); //B01000000;  

  interrupts(); // Enable interrupts

  // Load settings

  // Set initial state
  changeState(false);

  //start Serial
  Serial.begin(115200);
  Serial.println("# Levitator PID Controller V1.1");
  if(!read_settings()) {
    // Failed to read settings from eeprom
    // Set default values
    default_settings();
    // Store settings
    write_settings();
    Serial.println("# Unable to read settings");
    Serial.println("# Restoring default values");
  }

  // Setup control loop period
  ctrlPeriodCounter= settings.ctrlPeriod;

  // Disable report
  reportPeriod=0;
  reportCounter=0;
  reportFlag=false;

  // Arm timer2 and enable interrupts
  TCNT2 = TIMER2_1MS_DELAY;
  TIMSK2 |= _BV(TOIE2);

}




void loop()
{
  processCommand();

  if(reportFlag) {
    sendReport();
    reportFlag=false;
  }
}















