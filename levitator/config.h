#ifndef __CONFIG_H__
#define __CONFIG_H__


// Coil can be connected to pin 9 or 10
#define COIL_PIN    9 

// Define the Hall effect sensor analog pin
#define SENSOR_PIN  A0

// Define VUSB detect pin
#define VUSB_PIN    A1

// Define BT Enable pin
#define BTEN_PIN    2

// The Controller's state pin   (HIGH = Idle, LOW = ACTIVE)
#define LED_PIN     8


// Version of the EEPROM data.
// Change whenever you want to override default data stored in EEPROM.
#define SETTINGS_VERSION  4


// Default PID parameters
#define DEFAULT_SETPOINT      270     //Default target hall effect readout
#define DEFAULT_KP            1.75    //Default Kp, proportional gain parameter
#define DEFAULT_KD            23.5    //Default Kd, derivative gain parameter
#define DEFAULT_KI            0.0017  //Default Ki, integral gain parameter

// Maximum integral term allowed in computation.
#define MAX_INTEGRAL          20000


// Hall effect sensor filtering factor.
// A running average of the sensor reading is computed, 
// with the most recent reading making up 1/SENS_FILTER of the average.
#define DEFAULT_SENS_FILTER     2

// Hall effect sensor inverse reading.
#define DEFAULT_SENS_REVERSED   0

// Coil response filtering factor
// A running average of the PID coil response is computed, 
// with the most recent coil response making un 1/COIL_FILTER of the average.
#define DEFAULT_COIL_FILTER     2

// PID Update period in milliseconds
#define DEFAULT_CTRL_PERIOD     1

// Hall effect sensor readout value to trigger the ACTIVE mode.
// When a magnet is in range, the sensor should read below this value.
#define DEFAULT_ACTV_THRESHOLD  325

// ACTIVE mode timeout in milliseconds.
// The controller will return to IDLE mode after this amount of time, when the magnet is no more in range.
#define DEFAULT_ACTV_TIMEOUT    2000


// If you ever need to restrict the Coil's response range,
// Change the following values.
#define DEFAULT_COIL_MIN      0
#define DEFAULT_COIL_MAX      255
#define DEFAULT_COIL_MID      ((DEFAULT_COIL_MAX-DEFAULT_COIL_MIN)/2)




#if !(COIL_PIN == 9 || COIL_PIN == 10)
#error "Coil pin should be 9 or 10. Please check your configuration"
#endif



#endif

