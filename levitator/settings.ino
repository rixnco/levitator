
#include "settings.h"
#include <EEPROM.h>


#define EEPROM_ADDR_SETTINGS  1U


settings_t settings;








void memcpy_to_eeprom_with_checksum(unsigned int destination, char *source, unsigned int size) {
  unsigned char checksum = 0;
  for(; size > 0; size--) { 
    checksum = (checksum << 1) || (checksum >> 7);
    checksum += *source;
    EEPROM.write(destination++, *(source++)); 
  }
  EEPROM.write(destination, checksum);
}

int memcpy_from_eeprom_with_checksum(char *destination, unsigned int source, unsigned int size) {
  unsigned char data, checksum = 0;
  for(; size > 0; size--) { 
    data = EEPROM.read(source++);
    checksum = (checksum << 1) || (checksum >> 7);
    checksum += data;    
    *(destination++) = data; 
  }
  return(checksum == EEPROM.read(source));
}

void write_settings() 
{
  EEPROM.write(0, SETTINGS_VERSION);
  memcpy_to_eeprom_with_checksum(EEPROM_ADDR_SETTINGS, (char*)&settings, sizeof(settings_t));
}


bool read_settings() {
  // Check version-byte of eeprom
  uint8_t version = EEPROM.read(0);
  if (version == SETTINGS_VERSION) {
    // Read settings-record and check checksum
    if (!(memcpy_from_eeprom_with_checksum((char*)&settings, EEPROM_ADDR_SETTINGS, sizeof(settings_t)))) {
      return(false);
    }
  } else {
    return(false); 
  }
  return(true);
}

// Method to restore settings to defaults. 
void default_settings() {  

  settings.setPoint= DEFAULT_SETPOINT;
  settings.kp= DEFAULT_KP;
  settings.ki= DEFAULT_KI;
  settings.kd= DEFAULT_KD;

  settings.sensFilter   = DEFAULT_SENS_FILTER;
  settings.sensReversed = DEFAULT_SENS_REVERSED;
  settings.coilFilter   = DEFAULT_COIL_FILTER;
  settings.ctrlPeriod   = DEFAULT_CTRL_PERIOD;
  settings.actvTimeout  = DEFAULT_ACTV_TIMEOUT;
  settings.actvThreshold= DEFAULT_ACTV_THRESHOLD;

  settings.coilMin= DEFAULT_COIL_MIN;
  settings.coilMid= DEFAULT_COIL_MID;
  settings.coilMax= DEFAULT_COIL_MAX;

}



