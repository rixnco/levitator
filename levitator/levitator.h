#ifndef __LEVITATOR_H__
#define __LEVITATOR_H__


// The controller's state: TRUE= active , FALSE=idle
extern volatile bool ctrlState;

// The controller's report period in milliseconds
// 0= no report.
extern volatile unsigned long reportPeriod;


#endif

