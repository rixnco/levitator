/**
 * Implementation of the Levitator PID Controller serial protocol
 */

#include "Levitator.h"
#include "Config.h"
#include "Protocol.h"


// Serial Protocol definition
#define PROTO_REPORT_REQ    '?'
#define PROTO_PARAM_REQ     '$'
#define PROTO_PID_REQ       '!'



// Configuration parameters ID
// Used by the protocol handler
#define PARAM_SP              0
#define PARAM_KP              1
#define PARAM_KI              2
#define PARAM_KD              3
#define PARAM_SENS_FILTER     4
#define PARAM_SENS_REVERSED   5
#define PARAM_COIL_FILTER     6
#define PARAM_CTRL_PERIOD     7
#define PARAM_ACTV_THRESHOLD  8
#define PARAM_ACTV_TIMEOUT    9
#define PARAM_COIL_MIN        10
#define PARAM_COIL_MID        11
#define PARAM_COIL_MAX        12
#define PARAM_LAST            13

const char* PARAM_NAME[] = {
  "SP",
  "KP",
  "KI",
  "KD",
  "SENSFLTR",
  "SENSRVSD",
  "COILFLTR",
  "PERIOD",
  "THRSHLD",
  "TIMEOUT",
  "COILMIN",
  "COILMID",
  "COILMAX"
};

const char* STATE_NAME[] = {
  "IDLE",
  "ACTV"
};


char  inBuffer[64];
byte  received=0;



/**
 * Get Parameter ID based on its name.
 */
int getParamID(const char* str, char** endptr) {
  int l;
  int t;
  for(t=0; t<PARAM_LAST; ++t) {
    l= strlen(PARAM_NAME[t]);
    if(strncmp(str, PARAM_NAME[t], l)==0) {
      if(endptr!=NULL) *endptr=(char*)str+l;    
      break;
    }
  }
  if(t==PARAM_LAST) return -1;
  return t;
}



void sendParam(int p) {
  if(p<0 || p>=PARAM_LAST) return;
  Serial.print("$");
  Serial.print(PARAM_NAME[p]);
  Serial.print("=");
  switch(p) {
    case PARAM_SP: Serial.println(settings.setPoint); break;
    case PARAM_KP: Serial.println(settings.kp); break;
    case PARAM_KI: Serial.println(settings.ki,4); break;
    case PARAM_KD: Serial.println(settings.kd); break;
    case PARAM_SENS_FILTER: Serial.println(settings.sensFilter); break;
    case PARAM_SENS_REVERSED: Serial.println(settings.sensReversed); break;
    case PARAM_COIL_FILTER: Serial.println(settings.coilFilter); break;
    case PARAM_CTRL_PERIOD: Serial.println(settings.ctrlPeriod); break;
    case PARAM_ACTV_THRESHOLD: Serial.println(settings.actvThreshold); break;
    case PARAM_ACTV_TIMEOUT: Serial.println(settings.actvTimeout); break;
    case PARAM_COIL_MIN: Serial.println(settings.coilMin); break;
    case PARAM_COIL_MID: Serial.println(settings.coilMid); break;
    case PARAM_COIL_MAX: Serial.println(settings.coilMax); break;
    default: Serial.println("???");
  }
}

void sendSettings() {
  for(int t=0; t<PARAM_LAST; ++t) {
    sendParam(t);
  }
}

void sendReport() {
  int state, sensor, pwm;
  noInterrupts();
  state= ctrlState;
  sensor= currentSensor;
  pwm= coilPWM;
  interrupts();
  
  Serial.print(">"); Serial.print(STATE_NAME[state?1:0]); 
  Serial.print(","); Serial.print(settings.setPoint);
  Serial.print(","); Serial.print(sensor);
  Serial.print(","); Serial.println(pwm);
}

void sendError(char* msg) {
  Serial.print(">ERROR");
  if(msg!=NULL) {
    Serial.print(","); Serial.println(msg);
  } else {
    Serial.println(); 
  }
}

void sendAck() {
  Serial.println(">OK");
}


bool processReportRequest() {
  unsigned long period;
  char* ptr= &inBuffer[1];
  
  if(*ptr==0) {
    sendReport();
  } else {
    period= strtoul(ptr, &ptr, 10);
    if(*ptr!=0) return false;
    noInterrupts();
    reportPeriod=period;
    interrupts();
  }
  return true;
}


bool setParam(int p, char* ptr) {
  float f;
  unsigned long  ul;
  
  switch(p) {
    case PARAM_SP: 
      ul= strtoul(ptr, &ptr, 10);
      if(*ptr!=0) return false;
      noInterrupts();
      settings.setPoint=(int)ul;
      interrupts();
      break;
    case PARAM_KP: 
      f= strtod(ptr, &ptr);
      if(*ptr!=0) return false;
      noInterrupts();
      settings.kp=f;
      interrupts();
      break;
    case PARAM_KI: 
      f= strtod(ptr, &ptr);
      if(*ptr!=0) return false;
      noInterrupts();
      settings.ki=f;
      interrupts();
      break;
    case PARAM_KD: 
      f= strtod(ptr, &ptr);
      if(*ptr!=0) return false;
      noInterrupts();
      settings.kd=f;
      interrupts();
      break;
    case PARAM_SENS_FILTER: 
      ul= strtoul(ptr, &ptr, 10);
      if(*ptr!=0) return false;
      noInterrupts();
      settings.sensFilter=(unsigned int) ul;
      interrupts();
      break;
    case PARAM_SENS_REVERSED: 
      ul= strtoul(ptr, &ptr, 10);
      if(*ptr!=0) return false;
      noInterrupts();
      settings.sensReversed= (ul!=0);
      interrupts();
      break;
    case PARAM_COIL_FILTER: 
      ul= strtoul(ptr, &ptr, 10);
      if(*ptr!=0) return false;
      noInterrupts();
      settings.coilFilter=(unsigned int) ul;
      interrupts();
      break;
    case PARAM_CTRL_PERIOD: 
      ul= strtoul(ptr, &ptr, 10);
      if(*ptr!=0) return false;
      noInterrupts();
      settings.ctrlPeriod=ul;
      interrupts();
      break;
    case PARAM_ACTV_THRESHOLD: 
      ul= strtoul(ptr, &ptr, 10);
      if(*ptr!=0) return false;
      noInterrupts();
      settings.actvThreshold=(unsigned int) ul;
      interrupts();
      break;
    case PARAM_ACTV_TIMEOUT: 
      ul= strtoul(ptr, &ptr, 10);
      if(*ptr!=0) return false;
      noInterrupts();
      settings.actvTimeout=ul;
      interrupts();
      break;
    case PARAM_COIL_MIN: 
      ul= strtoul(ptr, &ptr, 10);
      if(*ptr!=0) return false;
      ul= constrain(ul, 0,255);
      noInterrupts();
      settings.coilMin=ul;
      if(ul>settings.coilMid) settings.coilMid=ul;
      if(ul>settings.coilMax) settings.coilMax=ul;
      coilPWM=0;
      interrupts();
      break;
    case PARAM_COIL_MID: 
      ul= strtoul(ptr, &ptr, 10);
      if(*ptr!=0) return false;
      ul= constrain(ul, 0,255);
      noInterrupts();
      settings.coilMid=ul;
      if(ul<settings.coilMin) settings.coilMin=ul;
      if(ul>settings.coilMax) settings.coilMax=ul;
      coilPWM=0;
      interrupts();
      break;
    case PARAM_COIL_MAX: 
      ul= strtoul(ptr, &ptr, 10);
      if(*ptr!=0) return false;
      ul= constrain(ul, 0,255);
      noInterrupts();
      settings.coilMax=ul;
      if(ul<settings.coilMin) settings.coilMin=ul;
      if(ul<settings.coilMid) settings.coilMid=ul;
      coilPWM=0;
      interrupts();
      break;
    default:
      return false;
  }
  return true;
}

bool processParamRequest() {
  char* ptr= &inBuffer[1];

  switch(*ptr) {
   case '$':
    ++ptr;
    if(*ptr==0) sendSettings();
    return true;
    break;
   case '<':
    // Store settings.
    noInterrupts();
    write_settings();
    interrupts();
    Serial.println(">SETTINGS,stored");
    return true;
    break;
   case '>':
    // restore settings
    bool b;
    noInterrupts();
    if(!(b=read_settings())) {
      default_settings();
      write_settings();
    }
    interrupts();
    if(b) Serial.println(">SETTINGS,restored");
    else Serial.println(">SETTINGS,default");
    return true;
    break;
  default:
    int p= getParamID(ptr, &ptr);
    if(p==-1) return false;
    if(*ptr==0) {
      sendParam(p);
    } else if(*ptr=='=') {
      ++ptr;
      if(*ptr==0 || !setParam(p, ptr)) return false;
      return true;
    }  
  }  
  return false;
}

bool processPIDRequest() {
  char* ptr= &inBuffer[1];
  char* nxt;
  unsigned long sp=settings.setPoint;
  float kp= settings.kp;
  float ki= settings.ki;
  float kd= settings.kd;

  // Read setPoint
  sp= strtoul(ptr, &nxt, 10);
  if(ptr==nxt) sp= settings.setPoint;  // missing setPoint value, use current value
  ptr=nxt;
  if(*ptr==',') {
    // We have another value
    // Read KP
    kp= strtod(++ptr,&nxt);
    if(ptr==nxt) kp= settings.kp; // missing kp value, use current value
    ptr=nxt;
    if(*ptr==',') {
      // We have another value
      // Read KI
      ki= strtod(++ptr,&nxt);
      if(ptr==nxt) ki= settings.ki; // missing ki value, use current value
      ptr=nxt;
      if(*ptr==',') {
        // We have another value
        // Read KD
        kd= strtod(++ptr,&nxt);
        if(ptr==nxt) kd= settings.kd; // missing kd value, use current value
        ptr=nxt;
      }
    }
  }    
  if(*ptr!=0) return false;

  noInterrupts();
  settings.setPoint= sp;
  settings.kp= kp;
  settings.ki= ki;
  settings.kd= kd;
  interrupts();
  
  
  return true;
}

void processCommand()
{
  if( !Serial.available() ) return;
   
  // Read Serial input.
  int c;
  c= Serial.read();
  if(c=='\n') {
    inBuffer[received++]=0;
    switch(inBuffer[0]) {
      case PROTO_REPORT_REQ:
        if(!processReportRequest()) sendError("Invalid report period");
        else sendAck();  
        break;
      case PROTO_PARAM_REQ:
        if(!processParamRequest()) sendError("Invalid parameter command");
        else sendAck();  
        break;
      case PROTO_PID_REQ:
        if(!processPIDRequest()) sendError("Invalid PID request");
        else sendAck();  
        break;
    }
    // Command processed.
    // Reset buffer
    received=0;
    inBuffer[0]=0;
  } else if(received<63) {
    // Ignore carriage returns, white spaces and tabs
    if(c!='\r' && c!=' ' && c!='\t' ) inBuffer[received++]= c;
  }
}


