![Levitator-Small.png](/doc/img/Levitator-400x240.png)

***
## Levitator
## Yet another Arduino Magnetic Levitation Project
***

This controller is implemented using a free running ADC converter, to follow the hall effect sensor readout in "real time"

Timer 2 is setup to sequence the controller at a 1ms rate.

The PID computation is performed every "ctrlPeriod" milliseconds (see config.h)

The control loop is thus totally independant from the main processing loop.

The rest of the time, the system checks and process the serial protocol.

Check "Config.h" for more details on configuring the Levitator

The Levitator protocol is a simple ASCII protocol, so you can configure and control the
Levitator using a simple serial terminal

Protocol:

* "$$"       : List all settings parameters
* "$xxx"     : print the parameter 'xxx'
* "$xxx=yyy" : set the the 'xxx' parameter's value to 'yyy'
* "$<"       : Write current settings to EEPROM. The Levitator will use those settings every time it is powerd on.
* "$>"       : Read settings from EEPROM.

* "![SP][,[KP][,[KI][,[KD]]]] : set the PID 'setPoint', 'kP', 'kI', 'kD' to the corresponding values at once.
                              Values between sqared brackets are optional.

* "?"        : Request the current controller state: >IDLE/ACTV,setpoint,sensor,coil
* "?x"       : Request a report every 'x' milliseconds. 0 to turn off reporting.

The controller acknoledge requests with
* ">OK"
or
* ">ERROR,msg"


Configuration parameters:

* "SP" : The target set point for the sensor value
* "KP" : The PID proportional term
* "KI" : The PID integral term
* "KD" : The PID derivative term
* "SENSFLTR" : The sensor value filter factor. A running average of the sensor reading is computed with the most recent value making up 1/SENSFLTR of the average
* "COILFLTR" : The coil response filter factor. A running average of the coil response is computed with the most recent value making up 1/COILFLTR of the average
* "PERIOD" : The PID update period in milliseconds. 
* "THRSHLD" : The magnetic threshold value to activate the ACTIVE mode. When a magnet is in range, the sensor should read below this value.
* "TIMEOUT" : The ACTIVE mode timeout in milliseconds. The controller will return to IDLE mode after this amount of time, when the magnet is no more in range.
* "COILMIN" : The minimum allowed coil response. 
* "COILMID" : The coil midpoint used to calculate the response.
* "COILMAX" : The maximum allowed coil response. 



For the mechanical and electronic parts please check out my [Thing](https://goo.gl/UQwSkJ)


Enjoy :-)